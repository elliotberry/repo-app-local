class apiClient {
  constructor(opts) {
    this.onFinishedCB = opts.onFinished || null;
    this.lastTimeUpdated = null;
    this.updateCB = opts.updateCB || null;
    this.debug = opts.debug || true;
    this.startTime = performance.now();
    this.statusMessage("Starting");
    this.bitbucketToken = null;
    this.db.init();
    this.init();
  }

  async init() {
    await this.statusMessage("Starting api updates...");
   // try {
      let lastDate = await this.db.whenUpdated();
      let lateDatea = Date.parse(lastDate);
      await this.statusMessage(`Last date updated: ${lateDatea}`);
      if (this.lessThanOneHourAgo(lateDatea)) {
        await this.statusMessage(
          "Got API results recently; pulling from localstorage"
        );
        let oo = await this.db.allArray();
        this.finishFunction(oo);
      } else {
        this.bitbucketToken = await this.getBitbucketToken();
        await this.statusMessage("Got bitbucket token");
        await this.getAllBitbucketRepos(lastDate);
        await this.statusMessage("Got all bitbucket repos");
        this.ghrepos = await this.getGithubRepos();
        await this.statusMessage("Got all github repos");
        let u = new Date();
        let y = u.toISOString();
        this.db.set("lastTimeUpdated", y);

        let oo = await this.db.allArray();
        this.finishFunction(oo);
      }
    //} catch (err) {
     // await this.statusMessage("Error: " + err);
   // }
  }

  lessThanOneHourAgo(date) {
    const HOUR = 1000 * 60 * 60;
    const anHourAgo = Date.now() - HOUR;

    return date > anHourAgo;
  }

  humanFileSize(bytes, si) {
    var thresh = si ? 1000 : 1024;
    if (Math.abs(bytes) < thresh) {
      return bytes + " B";
    }
    var units = si
      ? ["kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"]
      : ["KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"];
    var u = -1;
    do {
      bytes /= thresh;
      ++u;
    } while (Math.abs(bytes) >= thresh && u < units.length - 1);
    return bytes.toFixed(1) + " " + units[u];
  }
  async finishFunction(repos) {
    if (this.onFinishedCB && typeof this.onFinishedCB == "function") {
      this.onFinishedCB(repos);
      this.statusMessage("done.");
    }
  }
  async statusMessage(str) {
    let theStatus = str;
    if (this.debug) {
      let elapsed = performance.now() - this.startTime;
      theStatus = "[" + (elapsed / 1000).toFixed(5) + "s] " + str;
      console.log(theStatus);
    }
    if (this.updateCB && typeof this.updateCB == "function") {
      this.updateCB(theStatus);
    }
    this.status = theStatus;
  }
  async getGithubRepos() {
    var myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Basic ZWxsaW90YmVycnk6NDg1YzgwZmFlMmExN2UzMGNmNTFjZDE1ZDBjZjk1YjBmM2FiNTlmZg=="
    );

    var requestOptions = {
      method: "GET",
      headers: myHeaders,
      redirect: "follow",
    };

    let repos = await fetch(
      "https://api.github.com/user/repos",
      requestOptions
    );
    let ff = await repos.json();
    let k = await this.modifyGHObj(ff);
    return k;
  }

  async makeID(str) {
    const msgUint8 = new TextEncoder().encode(str); // encode as (utf-8) Uint8Array
    const hashBuffer = await crypto.subtle.digest("SHA-256", msgUint8); // hash the message
    const hashArray = Array.from(new Uint8Array(hashBuffer)); // convert buffer to byte array
    const hashHex = hashArray
      .map((b) => b.toString(16).padStart(2, "0"))
      .join(""); // convert bytes to hex string
    return hashHex;
  }

  async modifyGHObj(arr) {
    let that = this;
    let newArr = [];
    for await (let obj of arr) {
      let theID = await that.makeID(obj.id);
      let sizeStr = await that.humanFileSize(obj.size);
      let newObj = {
        id: theID,
        name: obj.name,
        owner: obj.owner.login,
        link: obj.html_url,
        provider: "github",
        size: obj.size,
        sizeString: sizeStr,
        lang: obj.language,
        time: {
          created: obj.created_at,
          updated: obj.updated_at,
        },
        description: obj.description,
        originalObject: obj,
      };
      that.db.set(newObj.id, JSON.stringify(newObj));
      newArr.push(newObj);
    }
    await this.statusMessage("Parsed GH repos");
    return newArr;
  }
  async getBitbucketRepoPage(url) {
    let init = {
      method: "GET",
      headers: {
        Authorization: "Bearer " + this.bitbucketToken,
        "Content-Type": "application/json",
      },
    };

    let response = await fetch(url, init);
    let ret = await response.json();
    await this.statusMessage("Got another page of bitbucket repos");
    return ret;
  }

  async getBitbucketToken() {
    let init = {
      body: "grant_type=client_credentials",
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization:
          "Basic ektiWHBEUEhrQmRkblZqdjdCOmdiN2NDSGZwNlJ3UGJWRXY2UnF4S3pzbWtlQ3JkdThu",
      },
    };

    let resp = await fetch(
      "https://bitbucket.org/site/oauth2/access_token",
      init
    );
    const y = await resp.json();
    return y.access_token;
  }
  async formatBitbucketReposArr(arr) {
    
    let r = arr.map(function(item) {
      return item.values
    });
    console.log(r)
  }
  async formatBitbucketRepos(arr) {
    console.log(arr)
    await this.formatBitbucketReposArr(arr)
  /*  let that = this;
    let newArr = [];
    for await (let obj of ret) {
      let theID = await that.makeID(obj.uuid);
      let sizeStr = await that.humanFileSize(obj.size);
      let newObj = {
        id: theID,
        name: obj.name,
        owner: obj.owner.nickname,
        link: obj.links.html.href,
        provider: "Bitbucket",
        size: obj.size,
        sizeString: sizeStr,
        lang: obj.language,
        time: {
          created: obj.created_on,
          updated: obj.updated_on,
        },
        description: obj.description,
        originalObject: obj,
      };
      that.db.set(newObj.id, JSON.stringify(newObj));
      newArr.push(newObj);
    }
    await this.statusMessage("Parsed bitbucket repos and put them in db");
    return newArr; */
  }

  async repoArrayPageMath(firstURL) {
    let u;
    let d = await this.getBitbucketRepoPage(firstURL);
    await this.statusMessage("Number of new bitbucket repos: " + d.size);
    if (d.size > 0) {
      if (d.size >= 100) {
        let numberOfpages = Math.ceil(d.size / d.pagelen);
        await this.statusMessage(
          "Got first page of bitbucket repos (1/" + numberOfpages + ")"
        );
        let z = [...Array(numberOfpages)]
          .map((x, i) => i + 1)
          .slice(1, numberOfpages.length);

        u = await Promise.all(
          z.map((item) =>
            this.getBitbucketRepoPage(
              "https://api.bitbucket.org/2.0/repositories/erberry?pagelen=100&page=" +
                item
            )
          )
        );
        u.push(d);
      } else {
        u = d;
      }
    }
    return u;
  }
  async getAllBitbucketRepos(timeStop) {
    let firstURL =
      "https://api.bitbucket.org/2.0/repositories/erberry?pagelen=100&page=1";
    if (timeStop) {
      let qr = encodeURIComponent(
        "updated_on >= " + timeStop + " OR created_on >= " + timeStop
      );
      firstURL =
        "https://api.bitbucket.org/2.0/repositories/erberry?pagelen=100&q=" +
        qr;
    }
    let u = await this.repoArrayPageMath(firstURL);
    console.log(u);
    await this.formatBitbucketRepos(u.values);
  }

  db = {
    init() {
      if (typeof Storage !== "undefined") {
        // console.log("localstorage");
      }
    },
    async allAsObject() {
      //get all repos as a keyed object
      let vals = {};
      let keys = Object.keys(window.localStorage);

      for await (let key of keys) {
        // if (this.isHex(key)) {
        try {
          let jj = await window.localStorage.getItem(key);
          vals[key] = JSON.parse(jj);
        } catch {
          console.log("not json");
        }
        // }
      }

      return vals;
    },
    async allArray() {
      //get all repos as array
      let vals = [];
      let keys = Object.keys(window.localStorage);

      for await (let key of keys) {
        try {
          let jj = await window.localStorage.getItem(key);
          vals.push(JSON.parse(jj));
        } catch {
          console.log("not json");
        }
      }

      return vals;
    },
    async whenUpdated() {
      let ret = "2011-11-11";
      let y = window.localStorage.getItem("lastTimeUpdated");
      if (y) {
        ret = y;
      }
      return ret;
    },
    async set(prop, val) {
      let k = await window.localStorage.setItem(prop, val);
      return k;
    },
    async get(prop) {
      let k = window.localStorage.getItem(prop);
      return k;
    },
    async remove(prop) {
      let k = window.localStorage.removeItem(prop);
      return k;
    },
  };
}
export default apiClient;
